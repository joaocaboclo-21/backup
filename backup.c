#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define ARQUIVO "bkp.bin"


struct record{
    unsigned int tamanho;
    char *file_name;
    char *data;
};

void printa_nome_tamanho(char *nome, int tam_nome, unsigned int tam_arquivo){
    int i;
    for (i = 0; i < tam_nome; i++){
        printf("%c", nome[i]);
    }
    printf(": %u\n", tam_arquivo);
}


int main(int argc, char *argv[])
{
    int flag_t = 0;
    int option;

    /*Verifica se a entrada foi ./backup com ou sem a flag -t*/
    while ((option = getopt (argc, argv, "t")) != -1){
        switch (option)
        {
            case 't':
                flag_t = 1;
                break;
            default:
                fprintf (stderr, "Uso: %s -t", argv[0]);
                exit(1);
        }
    }

    /*Abre arquivo em modo read*/
    FILE *arq;
    arq = fopen(ARQUIVO, "r");
    if (!arq)
    {
        perror ("Erro ao abrir arquivo");
        exit(1);
    }

    struct record record;
    unsigned int quantidade_registros, i;
    int tamanho_nome;
    record.file_name = malloc(sizeof(char)* 50);

    if (flag_t){
        /*Lê a quantidade de registros do arquivo*/
        fread(&quantidade_registros, sizeof(unsigned int), 1, arq);

        for (i = 0; i < quantidade_registros; i++){
            /*Le o tamanho do registro e nome*/
            fread(&record.tamanho, sizeof(unsigned int), 1, arq);
            fgets(record.file_name, 50, arq);
            tamanho_nome = strlen(record.file_name) - 1;

            /*Avanca o ponteiro para o inicio do proximo registro*/
            fseek (arq, record.tamanho - tamanho_nome - 1, SEEK_CUR);

            printa_nome_tamanho(record.file_name, tamanho_nome, record.tamanho);
        }
        free(record.file_name);
    }

    else{

        FILE *arq_criado;
        unsigned int tamanho_resto;

        /*Lê a quantidade de registros do arquivo*/
        fread(&quantidade_registros, sizeof(unsigned int), 1, arq);
        for (i = 0; i < quantidade_registros; i++){
            /*Le o tamanho do registro e nome*/
            fread(&record.tamanho, sizeof(unsigned int), 1, arq);
            fgets(record.file_name, 50, arq);

            /*Obtem o tamanho do nome e do resto dos registros*/
            tamanho_nome = strlen(record.file_name) - 1;
            tamanho_resto = record.tamanho - tamanho_nome - 1;

            /*Aloca espaço para o conteudo do registro*/
            record.data = malloc(sizeof(char) * tamanho_resto);
            fread(record.data, tamanho_resto, 1, arq);

            /*cria arquivo com o nome estabelecido nos registros*/
            arq_criado = fopen(record.file_name, "w");
            if (!arq_criado)
            {
                perror ("Erro ao abrir arquivo");
                exit(1);
            }

            /*Escreve no arquivo criado as inforcoes no tamanho de blocos necessario*/
            fwrite(record.data, tamanho_resto, 1, arq_criado);
            

            fclose(arq_criado); 
            free(record.data);

        }
        free(record.file_name);

    }

    return 0;
}